// libraries
import React from "react";
import { Route, Routes } from "react-router-dom";

// styles
import "./styles/sb-admin-2.min.css";

// components
import PrivateRoute from "./components/PrivateRoute";

// pages
import { Login } from "./pages/Account/Login";
import { Admin } from "./pages/Admin/Admin";
import Home from "./pages/Home/Home";
import AccountRoute from "./components/AccountRoute";

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route
        path="/login"
        element={
          <AccountRoute>
            <Login />
          </AccountRoute>
        }
      />
      <Route
        path="/admin"
        element={
          <PrivateRoute>
            <Admin />
          </PrivateRoute>
        }
      />
    </Routes>
  );
}

export default App;
