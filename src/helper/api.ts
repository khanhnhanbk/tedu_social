import axios from "axios";

const api = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

api.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response) {
      switch (error.response.status) {
        case 401:
          // TODO: handle 401 status
          break;
        case 404:
          // TODO: handle 404 status
          break;
        default:
          // TODO: handle 404 status
          break;
      }
    }
    return Promise.reject(error.response);
  }
);

export { api };
