import { RouteProps } from "react-router";
import { Navigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { AccountState } from "../store/account/types";
import { AppState } from "../store";

const PrivateRoute: React.FC<RouteProps> = ({ children }) => {
  const account: AccountState = useSelector((state: AppState) => state.account);
  console.log("🚀 ~ file: PrivateRoute.tsx:9 ~ account:", account);
  const { token } = account;
  if (!token) {
    return <Navigate to="/login" replace />;
  }

  return <>{children}</>;
};

export default PrivateRoute;
