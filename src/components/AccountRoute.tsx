import { RouteProps } from "react-router";
import { Navigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { AccountState } from "../store/account/types";
import { AppState } from "../store";

const AccountRoute: React.FC<RouteProps> = ({ children }) => {
  const account: AccountState = useSelector((state: AppState) => state.account);
  const { token } = account;
  if (!token) {
    return <>{children}</>;
  } else {
    return <Navigate to="/admin/" replace />;
  }
};

export default AccountRoute;
