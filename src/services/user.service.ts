import { api } from "../helper";

const login = async (email: string, password: string) => {
  const body = { email, password };
  const response = await api.post("/api/v1/auth", body);
  return response.data;
};

const logout = () => {
  sessionStorage.removeItem("user");
};

const getCurrentLoginUser = async () => {
  const response = await api.get("/api/v1/auth");
  return response.data;
};

export const userService = {
  login,
  logout,
  getCurrentLoginUser,
};
