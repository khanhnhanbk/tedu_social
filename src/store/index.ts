import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { accountReducer } from "./account/reducer";
import thunkMiddleware from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
import { setAuthToken } from "../helper/set-auth-token";

const rootReducer = combineReducers({ account: accountReducer });
const persistConfig = {
  key: "root",
  storage,
  whitelist: ["account"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export type AppState = ReturnType<typeof rootReducer>;
const composeEnhancers =
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const configureStore = () => {
  const middleware = [thunkMiddleware];
  const middlewareEnhance = applyMiddleware(...middleware);

  return createStore(persistedReducer, composeEnhancers(middlewareEnhance));
};
const store = configureStore();
const persistor = persistStore(store);

let currentState = store.getState() as AppState;

store.subscribe(() => {
  const previousState = currentState;
  currentState = store.getState() as AppState;

  if (previousState.account.token !== currentState.account.token) {
    const token = currentState.account.token;
    if (token) {
      setAuthToken(token);
    }
  }
});
export { store, persistor };
