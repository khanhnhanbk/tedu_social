import { Dispatch } from "react";
import {
  AccountActionType,
  GET_CURRENT_USER_FAILURE,
  GET_CURRENT_USER_REQUEST,
  GET_CURRENT_USER_SUCCESS,
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT,
} from "./types";
import { userService } from "../../services";
import { history } from "../../helper";
import { ThunkAction } from "redux-thunk";
import { AppState } from "..";

export const login = (
  email: string,
  password: string,
  from = "/"
): ThunkAction<Promise<void>, AppState, null, AccountActionType> => {
  return async (dispatch: Dispatch<AccountActionType>) => {
    dispatch({
      type: LOGIN_REQUEST,
      payload: {
        email: email,
        password: password,
      },
    });

    try {
      const res = await userService.login(email, password);
      dispatch({
        type: LOGIN_SUCCESS,
        payload: { token: res.token },
      });
      history.push(from);
    } catch (err) {
      console.log("🚀 ~ file: actions.ts ~ err", err);
      dispatch({
        type: LOGIN_FAILURE,
        payload: { error: i.response?.data.message || err.message },
      });
    }
  };
};

export const getCurrentLoginUser = () => {
  return async (dispatch: Dispatch<AccountActionType>) => {
    dispatch({
      type: GET_CURRENT_USER_REQUEST,
    });

    try {
      const res = await userService.getCurrentLoginUser();
      dispatch({
        type: GET_CURRENT_USER_SUCCESS,
        payload: { user: res },
      });
      return res;
    } catch (err) {
      console.log("🚀 ~ file: actions.ts ~ err", err);
      dispatch({
        type: GET_CURRENT_USER_FAILURE,
        payload: { error: err.response?.data.message || err.message },
      });
      return null;
    }
  };
};

export const logout = (): AccountActionType => ({ type: LOGOUT });
