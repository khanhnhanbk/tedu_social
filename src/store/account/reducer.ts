import {
  AccountActionType,
  AccountState,
  GET_CURRENT_USER_FAILURE,
  GET_CURRENT_USER_REQUEST,
  GET_CURRENT_USER_SUCCESS,
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT,
} from "./types";
const initialState: AccountState = {
  user: null,
  loading: false,
  error: null,
  token: null,
};
const accountReducer = (
  state: AccountState = initialState,
  action: AccountActionType
): AccountState => {
  switch (action.type) {
    case LOGIN_REQUEST:
    case GET_CURRENT_USER_REQUEST: {
      return {
        ...state,
        loading: true,
        error: null,
      };
    }

    case LOGIN_FAILURE:
    case GET_CURRENT_USER_FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    }
    case LOGOUT: {
      return {
        user: null,
        loading: false,
        token: null,
        error: null,
      };
    }
    case LOGIN_SUCCESS: {
      return {
        ...state,
        loading: false,
        token: action.payload.token,
      };
    }
    case GET_CURRENT_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        user: action.payload.user,
        error: null,
      };
    }

    default:
      return state;
  }
};

export { accountReducer };
