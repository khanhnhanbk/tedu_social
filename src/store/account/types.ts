// constants actions name
export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

// get current user
export const GET_CURRENT_USER_REQUEST = "GET_CURRENT_USER_REQUEST";
export const GET_CURRENT_USER_SUCCESS = "GET_CURRENT_USER_SUCCESS";
export const GET_CURRENT_USER_FAILURE = "GET_CURRENT_USER_FAILURE";

export const LOGOUT = "LOGOUT";

// Authentication interface
export interface AuthenticatedUser {
  _id: string;
  first_name: string;
  last_name: string;
  email: string;
  avatar: string;
}

interface LoginRequest {
  type: typeof LOGIN_REQUEST;
  payload: {
    email: string;
    password: string;
  };
}

interface LoginSuccess {
  type: typeof LOGIN_SUCCESS;
  payload: {
    token: string;
  };
}

interface LoginFailure {
  type: typeof LOGIN_FAILURE;
  payload: {
    error: string;
  };
}
interface GetCurrentUserRequest {
  type: typeof GET_CURRENT_USER_REQUEST;
}

interface GetCurrentUserSuccess {
  type: typeof GET_CURRENT_USER_SUCCESS;
  payload: {
    user: AuthenticatedUser;
  };
}

interface GetCurrentUserFailure {
  type: typeof GET_CURRENT_USER_FAILURE;
  payload: {
    error: string;
  };
}
interface Logout {
  type: typeof LOGOUT;
}

export interface AccountState {
  user: AuthenticatedUser | null;
  loading: boolean;
  error: string | null;
  token: string | null;
}
export type AccountActionType =
  | LoginRequest
  | LoginFailure
  | LoginSuccess
  | GetCurrentUserRequest
  | GetCurrentUserSuccess
  | GetCurrentUserFailure
  | Logout;
